package by.minsler;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

import java.util.Date;


/**
 * User: dzmitry.misiuk
 * Date: 11/19/12
 * Time: 7:15 PM
 */
@Entity
public class MyDate {

    @Id
    private Long id;

    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }



}